﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.NotificationsManager.Models
{
    public class UserNotification
    {
        public string Email { get; set; }
        public string Phone { get; set; }
        public IEnumerable<ParkingPlaceModel> ParkingPlaceModels { get; set; }
    }
}
