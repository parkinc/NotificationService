﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.NotificationsManager.Models.Configuration
{
    public class Service
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
