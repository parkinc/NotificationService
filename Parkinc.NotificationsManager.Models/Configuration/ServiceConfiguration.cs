﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.NotificationsManager.Models.Configuration
{
    public class ServiceConfiguration
    {
        public IEnumerable<Service> Services { get; set; }
    }
}
