﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.NotificationsManager.Models
{
    public class ParkingPlaceModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int FreeParkingPlaces { get; set; }
        public int AllParkingPlaces { get; set; }
    }
}
