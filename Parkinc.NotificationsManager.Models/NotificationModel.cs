﻿using System;
using System.Collections.Generic;

namespace Parkinc.NotificationsManager.Models
{
    public class NotificationModel
    {
        public string Email { get; set; }
        public string Phone { get; set; }
        public ParkingPlaceModel ParkingPlace { get; set; }
    }
}
