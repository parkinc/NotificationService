﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.NotificationManager.DTOs
{
    public class ParkingDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int FreeCount { get; set; }
        public int MaxCount { get; set; }
    }
}
