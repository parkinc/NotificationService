﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.NotificationManager.DTOs
{
    public class DateTimeDTO
    {
        public string DateTime { get; set; }
        public string Format { get; set; }
    }
}
    