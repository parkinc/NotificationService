﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.NotificationManager.DTOs
{
    //todo: move to SMS service
    public class SmsDTO
    {
        public string Receiver { get; set; }
        public string Content { get; set; }
    }
}
