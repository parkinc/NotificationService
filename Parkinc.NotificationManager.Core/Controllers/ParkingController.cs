﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Parkinc.NotificationManager.Core.Communication;
using Parkinc.NotificationManager.Core.Controllers.Interfaces;
using Parkinc.NotificationManager.DTOs;
using Parkinc.NotificationsManager.Models.Configuration;

namespace Parkinc.NotificationManager.Core.Controllers
{
    public class ParkingController : IParkingController
    {
        private readonly ICommunication _communication;
        private readonly IOptions<ServiceConfiguration> _serviceConfiguration;

        public ParkingController(ICommunication communication, IOptions<ServiceConfiguration> serviceConfiguration)
        {
            _communication = communication;
            _serviceConfiguration = serviceConfiguration;
        }

        public async Task<IEnumerable<ParkingDTO>> GetParkingDataAsync()
        {
            //return GenerateParkingData();

            var url = _serviceConfiguration.Value.Services.First(s => s.Name == "Parking-service").Url;
            var response = await _communication.GetRequestAsync(url);
            return JsonConvert.DeserializeObject<IEnumerable<ParkingDTO>>(response);
        }

        private IEnumerable<ParkingDTO> GenerateParkingData()
        {
            var list = new List<ParkingDTO>();

            for (var i = 0; i < 100; i++)
            {
                var all = new Random().Next(10, 500);
                var free = new Random().Next(0, all);

                list.Add(new ParkingDTO()
                {
                    Id = (i + 1),
                    Name = $"Parking Place {i + 1}",
                    MaxCount = all,
                    FreeCount = free
                });
            }

            return list;
        }
    }
}
