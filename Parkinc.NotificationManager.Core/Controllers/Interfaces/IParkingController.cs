﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Parkinc.NotificationManager.DTOs;

namespace Parkinc.NotificationManager.Core.Controllers.Interfaces
{
    public interface IParkingController
    {
        Task<IEnumerable<ParkingDTO>> GetParkingDataAsync();
    }
}
    