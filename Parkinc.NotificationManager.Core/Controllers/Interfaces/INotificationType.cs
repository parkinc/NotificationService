﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Parkinc.NotificationManager.DTOs;
using Parkinc.NotificationsManager.Models;

namespace Parkinc.NotificationManager.Core.Controllers.Interfaces
{
    public interface INotificationType<T>
    {
        T CreateDto(UserNotification notification);
        Task Send(T dto);
    }
}
