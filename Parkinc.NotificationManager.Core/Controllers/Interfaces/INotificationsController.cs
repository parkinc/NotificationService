﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Parkinc.NotificationManager.DTOs;
using Parkinc.NotificationsManager.Models;

namespace Parkinc.NotificationManager.Core.Controllers.Interfaces
{
    public interface INotificationController
    {
        Task SendNotificationsAsync(IEnumerable<UserNotification> notifications);

        Task<IEnumerable<NotificationDTO>> GetNotificationsDataAsync(DateTime lastFetchDate);
        IEnumerable<NotificationModel> GetNotificationsAsync(IEnumerable<NotificationDTO> notificationData, IEnumerable<ParkingDTO> parkingData);
        IEnumerable<UserNotification> GetUserNotifications(IEnumerable<NotificationModel> notifications);
    }
}
