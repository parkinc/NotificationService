﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using EasyNetQ;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Parkinc.Mail.DTOs;
using Parkinc.NotificationManager.Core.Communication;
using Parkinc.NotificationManager.Core.Controllers.Interfaces;
using Parkinc.NotificationManager.DTOs;
using Parkinc.NotificationsManager.Models;
using Parkinc.NotificationsManager.Models.Configuration;

namespace Parkinc.NotificationManager.Core.Controllers
{
    public class NotificationController : INotificationController
    {
        private readonly INotificationType<MailDTO> _emailController;
        private readonly INotificationType<SmsDTO> _smsController;
        private readonly ICommunication _communication;
        private readonly IOptions<ServiceConfiguration> _servicesConfiguration;

        public NotificationController(INotificationType<MailDTO> emailController,
            INotificationType<SmsDTO> smsController, ICommunication communication,
            IOptions<ServiceConfiguration> servicesConfiguration)
        {
            _emailController = emailController;
            _smsController = smsController;
            _communication = communication;
            _servicesConfiguration = servicesConfiguration;
        }

        public async Task SendNotificationsAsync(IEnumerable<UserNotification> notifications)
        {
            foreach (var notification in notifications)
            {
                if (notification.Email != string.Empty)
                {
                    var dto = _emailController.CreateDto(notification);
                    await _emailController.Send(dto);
                }

                //todo: implement SMS
                /*if (notification.Phone != string.Empty)
                {
                    var dto = _smsController.CreateDto(notification);
                    await _smsController.Send(dto);
                }*/
            }
        }

        public async Task<IEnumerable<NotificationDTO>> GetNotificationsDataAsync(DateTime lastFetchDate)
        {
            var url = _servicesConfiguration.Value.Services.First(x => x.Name == "Notification-service").Url;
            var urlParams =
                $"?lastNotifiedHour={lastFetchDate.Hour}&lastNotifiedMinute={lastFetchDate.Minute}";

            var response = await _communication.GetRequestAsync(url + urlParams);
            return JsonConvert.DeserializeObject<IEnumerable<NotificationDTO>>(response);
        }

        public IEnumerable<NotificationModel> GetNotificationsAsync(IEnumerable<NotificationDTO> notificationData, IEnumerable<ParkingDTO> parkingData)
        {
            var notifications = new List<NotificationModel>();

            foreach (var notificationDataRow in notificationData)
            {
                foreach (var parkingDataRow in parkingData)
                {
                    if (parkingDataRow.Id != notificationDataRow.ParkingPlaceId) continue;

                    notifications.Add(CreateNotificationModel(notificationDataRow, parkingDataRow));
                }
            }
          
            return notifications;
        }

        public IEnumerable<UserNotification> GetUserNotifications(IEnumerable<NotificationModel> notifications)
        {
            return notifications.GroupBy(
                n => new { n.Email, n.Phone },
                n => n.ParkingPlace,
                (key, g) => new UserNotification { Email = key.Email, Phone = key.Phone, ParkingPlaceModels = g.ToList() });

        }

        private static NotificationModel CreateNotificationModel(NotificationDTO notificationDto, ParkingDTO parkingDto)
        {
            var parkingModel = new ParkingPlaceModel
            {
                Id = parkingDto.Id,
                Name = parkingDto.Name,
                AllParkingPlaces = parkingDto.MaxCount,
                FreeParkingPlaces = parkingDto.FreeCount
            };

            return new NotificationModel
            {
                Email = notificationDto.Email,
                Phone = notificationDto.Phone,
                ParkingPlace = parkingModel,               
            };
        }

        private IEnumerable<NotificationDTO> GenerateNotificationData()
        {
            var list = new List<NotificationDTO>();

            for (var i = 0; i < 100; i++)
            {
                var email = "miropakanec@gmail.com";

                if (i % 3 == 0)
                {
                    email = "miropakanec@gmail.com";
                }

                list.Add(new NotificationDTO()
                {
                    Id = (i + 1),
                    ParkingPlaceId = (i + 1),
                    Email = email,
                    Phone = $"+45666666666"
                });
            }

            return list;
        }
    }
}
