﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyNetQ;
using EasyNetQ.NonGeneric;
using Parkinc.Mail.DTOs;
using Parkinc.NotificationManager.Core.Controllers.Interfaces;
using Parkinc.NotificationManager.DTOs;
using Parkinc.NotificationsManager.Models;

namespace Parkinc.NotificationManager.Core.Controllers
{
    public class EmailController : INotificationType<MailDTO>
    {
        private readonly IBus _bus;

        public EmailController(IBus bus)
        {
            _bus = bus;
        }

        public MailDTO CreateDto(UserNotification notification)
        {
            var parkingDtos = notification.ParkingPlaceModels.Select(parkingPlace => new ParkingPlaceDTO
            {
                Name = parkingPlace.Name,
                AllParkingPlaces = parkingPlace.AllParkingPlaces,
                FreeParkingPlaces = parkingPlace.FreeParkingPlaces
            }).ToList();

            return new MailDTO()
            {
                RecieverEmail = notification.Email,
                ParkingPlaces = parkingDtos,
                WithAds = true
            };
        }

        public async Task Send(MailDTO dto)
        {
            Console.WriteLine("Publishing a message to Email queue...");
            await _bus.PublishAsync(dto);
        }
    }
}
    