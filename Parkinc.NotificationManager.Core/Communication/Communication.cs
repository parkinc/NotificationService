﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Parkinc.NotificationManager.Core.Communication
{
    public class Communication : ICommunication
    {
        public async Task<string> GetRequestAsync(string url)
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode == false) return "";

                return await response.Content.ReadAsStringAsync();
            }
        }
    }
}
