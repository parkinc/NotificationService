﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Parkinc.NotificationManager.Core.Communication
{
    public interface ICommunication
    {
        Task<string> GetRequestAsync(string url);
    }
}
