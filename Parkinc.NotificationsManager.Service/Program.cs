﻿using System;
using EasyNetQ;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Parkinc.NotificationManager.Core.Controllers;
using Parkinc.NotificationManager.Core.Controllers.Interfaces;
using Parkinc.NotificationsManager.Service.Endpoints;
using IServiceProvider = System.IServiceProvider;

namespace Parkinc.NotificationsManager.Service
{
    class Program
    {
        private static IConfiguration _configuration;
        private static IServiceProvider _serviceProvider;

        static void Main(string[] args)
        {
            Startup();

            var endpoint = _serviceProvider.GetService<IEndpoint>();
            endpoint.Start();
        }

        private static void Startup()
        {
            Console.WriteLine("Starting notification manager service...");
            _configuration = Configure.AppSettings("appsettings.json");
            _serviceProvider = Configure.Dependencies(_configuration);
        }
    }
}
