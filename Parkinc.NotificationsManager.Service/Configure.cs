﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using EasyNetQ;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Parkinc.Mail.DTOs;
using Parkinc.NotificationManager.Core.Communication;
using Parkinc.NotificationManager.Core.Controllers;
using Parkinc.NotificationManager.Core.Controllers.Interfaces;
using Parkinc.NotificationManager.DTOs;
using Parkinc.NotificationsManager.Models.Configuration;
using Parkinc.NotificationsManager.Service.Endpoints;

namespace Parkinc.NotificationsManager.Service
{
    public static class Configure
    {
        public static IConfiguration AppSettings(string filePath)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(filePath, true, true);

            return builder.Build();
        }

        public static ServiceProvider Dependencies(IConfiguration configuration)
        {
            var serviceProvider = new ServiceCollection()

                .AddSingleton<INotificationController, NotificationController>()
                .AddSingleton<IParkingController, ParkingController>()
                .AddSingleton<INotificationType<MailDTO>, EmailController>()
                .AddSingleton<INotificationType<SmsDTO>, SmsController>()
                .AddSingleton<IEndpoint, Endpoint>()
                .AddSingleton<ICommunication, Communication>()
                .AddSingleton(RabbitHutch.CreateBus(configuration["MessagingProvider:ConnectionString"]))
                .Configure<ServiceConfiguration>(configuration.GetSection("ServiceConfiguration"))
                .AddOptions()
                .BuildServiceProvider();

            return serviceProvider;
        }

    }
}
