﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EasyNetQ;
using Parkinc.NotificationManager.Core.Controllers.Interfaces;
using Parkinc.NotificationManager.DTOs;

namespace Parkinc.NotificationsManager.Service.Endpoints
{
    public class Endpoint : IEndpoint
    {
        private readonly IBus _bus;
        private readonly INotificationController _notificationController;
        private readonly IParkingController _parkingController;
        private bool _isRunning;
        private ISubscriptionResult _subscription;

        public Endpoint(IBus bus, INotificationController notificationController, IParkingController parkingController)
        {
            _bus = bus;
            _notificationController = notificationController;
            _parkingController = parkingController;
            _isRunning = false;
        }

        public void Start()
        {
            _isRunning = true;
            SubscribeAsync();
        }

        public void Stop()
        {
            _subscription.SafeDispose();
            _isRunning = false;
        }

        public bool IsRunning()
        {
            return _isRunning;
        }

        public string SubscriptionId => "Parkinc.NotificationsEndpoint.Service";

        private void SubscribeAsync()
        {
            _subscription = _bus.SubscribeAsync<DateTimeDTO>(SubscriptionId, async message =>
            {
                Console.WriteLine(
                    $"Receiving a message (at {DateTime.Now}): Datetime - {message.DateTime}, Format - {message.Format}.");

                var dateTime = DateTime.ParseExact(message.DateTime, message.Format, CultureInfo.CurrentCulture).AddHours(1);

                Console.WriteLine($"Translating to datetime - {dateTime}.");

                var notificationData = await _notificationController.GetNotificationsDataAsync(dateTime);
                var parkingData = await _parkingController.GetParkingDataAsync();

                var notifications = _notificationController.GetNotificationsAsync(notificationData, parkingData);

                Console.WriteLine(
                    $"Collected {notificationData.Count()} notifications and {parkingData.Count()} parking spaces.");
                Console.WriteLine($"Found {notifications.Count()} matches.");

                var userNotifications = _notificationController.GetUserNotifications(notifications);

                await _notificationController.SendNotificationsAsync(userNotifications);
            });
        }
    }
}
