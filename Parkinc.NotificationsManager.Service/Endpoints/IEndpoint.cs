﻿using EasyNetQ;

namespace Parkinc.NotificationsManager.Service.Endpoints
{
    public interface IEndpoint
    {
        void Start();
        void Stop();
        bool IsRunning();
        string SubscriptionId { get; }
    }
}
