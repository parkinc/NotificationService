FROM microsoft/aspnetcore:latest
COPY release /app
WORKDIR /app
ENTRYPOINT ["dotnet", "Parkinc.NotificationsManager.Service.dll"]